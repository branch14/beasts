# beasts

A simulation of natural selection from the 80ies, which I implemented
in 1997 in Pascal, 1998 in Java and 2018 in ClojureScript.

## Setup

To get an interactive development environment run:

    lein figwheel

and open your browser at [localhost:3449](http://localhost:3449/).
This will auto compile and send all changes to the browser without the
need to reload. After the compilation process is complete, you will
get a Browser Connected REPL.

## The `demo` branch

The branch `demo` is what I used for the demo at the clojure.zh Meetup
on February 27th 2018. It works with Emacs and the Yasnippets
package. Open `src/beasts/core.cljs` and you will find some basic
instructions.

Please be aware that not all steps are written down yet. There are
some tweaks I did from memory.

## License

Copyright © 2018 Phil Hofmann

Distributed under the Eclipse Public License either version 1.0 or (at
your option) any later version.
