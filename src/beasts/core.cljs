(ns beasts.core
  (:require [reagent.core :as reagent :refer [atom]]))

(enable-console-print!)

(def width 256)
(def height 256)

(def puberty 100)
(def nourished 500)
(def nutritions 60)

(def initial-beasts 20)
(def initial-food 1000)

(defn make-beast []
  {:x (rand-int width)
   :y (rand-int height)
   :r 2
   :genes (vec (repeatedly 9 #(rand-int 10)))
   :energy 500
   :age 0})

(defn make-food []
  {:x (rand-int width)
   :y (rand-int height)
   :r 1
   :energy nutritions})

(defn pos [beast]
  (str (:x beast) "x" (:y beast)))

(defn add-food [food]
  (let [new-food (make-food)]
    (assoc food (pos new-food) new-food)))

(defonce world
  (atom {:beasts (repeatedly initial-beasts make-beast)
         :food (reduce add-food {} (range initial-food))}))

(defn circle [entity]
  [:circle {:cx (:x entity)
            :cy (:y entity)
            :r  (:r entity)}])

(defn visual []
  [:svg {:width 500 :height 500 :viewBox (str "0 0 " width " " height)
         :xmlns "http://www.w3.org/2000/svg"}
   (into [:g {:fill "green"}] (map circle (vals (:food @world))))
   (into [:g {:fill "red"}] (map circle (:beasts @world)))])

(reagent/render-component [visual]
                          (. js/document (getElementById "app")))

(def delta
  [[-1  1] [0  1] [1  1]
   [-1  0] [0  0] [1  0]
   [-1 -1] [0 -1] [1 -1]])

(defn options [beast]
  (->> (:genes beast)
       (map-indexed #(repeat %2 %1))
       (apply concat)))

(defn pick [options]
  (-> options shuffle first))

(defn move [beast]
  (let [dir (pick (options beast))
        [x y] (delta dir)]
    (-> beast
        (update :x + x)
        (update :y + y))))

(defn eat [beast food]
  (if-let [meal (food (pos beast))]
    (update beast :energy + (:energy meal))
    beast))

(defn mutate [genes]
  (let [op (if (> 0.5 (rand)) inc dec)
        idx (rand-int (count genes))]
    (update genes idx op)))

(defn make-offspring [beast]
  (-> beast
      (assoc :age 0)
      (update :energy #(int (/ % 2)))
      (update :genes mutate)))

(defn die [beast]
  (if (= 0 (:energy beast))
    nil
    beast))

(defn procreate [beast]
  (if (nil? beast)
    []
    (if (and (> (:age beast) puberty)
             (> (:energy beast) nourished))
      [(make-offspring beast)
       (make-offspring beast)]
      [beast])))

(defn process-beast [beast world]
  (let [food (:food world)]
    (-> beast
        move
        (eat food)
        (update :energy dec) ; exhaust
        (update :age inc) ; age
        (update :x mod width) ; wrap x
        (update :y mod height) ; wrap y
        die
        procreate)))

(defn process-beasts [beasts world]
  (mapcat #(process-beast % world) beasts))

(defn remove-meals [world]
  (-> world
      (update :food #(reduce dissoc % (map pos (:beasts world))))))

(defn tick [world]
  ;;(.log js/console (first (:beasts world)))
  (-> world
      (update :beasts process-beasts world)
      remove-meals
      (update :food add-food) ;; 1
      ))

(defn run []
  (swap! world tick)
  (.requestAnimationFrame js/window run))

(defonce start (run))

(defn on-js-reload []
  ;; optionally touch your app-state to force rerendering depending on
  ;; your application
  ;; (swap! app-state update-in [:__figwheel_counter] inc)
  )
